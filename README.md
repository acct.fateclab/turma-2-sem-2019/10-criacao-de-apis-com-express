# Typescript

Typescript é uma linguagem de programação provinda do Javascript, então todos as features dele existem aqui também.

Para começar, garanta que você possui o Typescript instalado, rode `npm install --global typescript` para ter acesso às features.

## Getting Started

In your editor, type the following JavaScript code in greeter.ts:

``` javascript
function greeter(person) {
    return "Hello, " + person;
}

let user = "Jane User";

document.body.textContent = greeter(user);
```

## Compilando o código

Estamos usando um arquivo .ts, mas no final das contas é um código Javascript, então é só transpilar esse código para rodarmos em Node.

`tsc greeter.ts`

Isso vai gerar um arquivo Javascript com o código transpilado do arquivo .ts que você escreveu.

Agora você consegue aproveitar features do Typescript, adicione o tipo no parametro da função:

``` javascript
function greeter(person: string) {
    return "Hello, " + person;
}

let user = "Jane User";

document.body.textContent = greeter(user);
```

Mas você não fará esse comando em todos os arquivos do teu Endpoint, você rodará apenas uma vez o comando e poderá "buildar" (transpilar todos os arquivos para Javascript) e/ou dar "watch" (assistir e aguardar uma alteração nos arquivos para buildar parcialmente os arquivos). [Veja mais nas documentações abaixo.](#Docs)

## Notações de tipo

Os tipos em Typescript são maneiras mais leves de fazer com que o compilador entenda o que deve ser passado para uma função ou variavel. Isso serve para que cada vez mais, possamos especificar como nossos métodos e váriaveis funcionam e também temos a segurança de que os métodos não falharão.

``` javascript
function greeter(person: string) {
    return "Hello, " + person;
}

const user = [0,1,2];

console.log(greeter(user));
```

O exemplo acima da um erro na compilação, pois antes mesmo de executar é identificado que a função espera uma string e não um array de números.

### Docs
- [Como compilar o Typescript para Javascript](https://code.visualstudio.com/docs/typescript/typescript-compiling)
- [Typescript em 5 minutos](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html)

# Criação de APIs com Express

O Express é um framework para aplicativo da web do Node.js mínimo e flexível que fornece um conjunto robusto de recursos para aplicativos web e móvel.

https://expressjs.com/pt-br/

Você também possui o Gerador de Arquivos do Express para te ajudar a criar o seus diretórios, pense nele como um `create-react-app` do back-end.

https://expressjs.com/pt-br/starter/generator.html

## Roteamento
O Roteamento refere-se à definição de terminais do aplicativo (URIs) e como eles respondem às solicitações do cliente. Para obter uma introdução a roteamento, consulte Roteamento básico.

O código a seguir é um exemplo de uma rota muito básica.

``` javascript
var express = require('express');
var app = express();

// respond with "hello world" when a GET request is made to the homepage
app.get('/', function(req, res) {
  res.send('hello world');
});
```

Métodos de roteamento
Um método de roteamento é derivado a partir de um dos métodos HTTP, e é anexado a uma instância da classe express.

o código a seguir é um exemplo de rotas para a raiz do aplicativo que estão definidas para os métodos GET e POST.

``` javascript
// GET method route
app.get('/', function (req, res) {
  res.send('GET request to the homepage');
});

// POST method route
app.post('/', function (req, res) {
  res.send('POST request to the homepage');
});
```

# Desafio

A partir disso, vocês criarão uma API igual a do desafio anterior, porem deverão tipificar todas as variaveis com seus respectivos tipos primitivos, são eles:

- number
- string
- boolean
- Array (representado pelo `[]` ao final do tipo, ex: `string[]`, é um array de strings)
- enum
- any (evitar ao máximo!)
- void

Alem de tipificar, vocês deverão ter as regras do "httpstat" como lógica nos endpoints de vocês, são elas:

- Criar dois endpoints:
  - Endpoint GET 1: Retornar um JSON com o Status Code e o Status Message, ex: `{ code: 200, message: "OK" }` 
  - Endpoint GET 2: Alem de retornar o JSON acima, ter uma configuração de **querystring** para colocar o endpoint em "sleep", ex: `/sleep?time=5000`
- Nos dois endpoints, qualquer valor que não esteja listado no site do "httpstat", deve retornar o JSON: `{ code: 404, message: "Esse valor não é um status válido." }`
- Transforme os arquivos Javascript em Typescript e remova todos os pontos que não são do Typescript para que funcione, ex: `require(...)`, `var`...
- Limpe o repositório, deixe apenas arquivos e código relativo ao Desafio.